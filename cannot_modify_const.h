#include <alloca.h>
#include <stdlib.h>

#include "../poly.h"
#include "helper_functions_from_example.h"

void CannotModifyConst() {
  const int N = 1000;
  srand(2137 * 2137);

  Mono *arr = (Mono *)alloca(N * sizeof(Mono));
  Mono *arr2 = (Mono *)alloca(N * sizeof(Mono));
  Poly *polys = (Poly *)alloca(N * sizeof(Poly));

  for (int i = 0; i < N; i++) {
    polys[i] = PolyFromCoeff(rand() % N + 1);
    arr[i] = arr2[i] = MonoFromPoly(&polys[i], rand() % N);
  }

  Poly p = PolyAddMonos(N, arr);

  for (int i = 0; i < N; i++) {
    assert(PolyIsEq(&arr[i].p, &arr2[i].p) && arr[i].exp == arr2[i].exp);
  }

  PolyDestroy(&p);
  for (int i = 0; i < N; i++) PolyDestroy(&polys[i]);
}
